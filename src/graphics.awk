BEGIN {
    baseline = philosopher_id * separator; 
    topline = philosopher_id * separator + height;
    print "\n#m=1";
    points[count++]=(0 " " baseline);
    print 0, baseline;
}
match($0, /Second ([0-9]+): philosopher ([0-9]+) is hungry/, ary) {
    if (ary[2] == philosopher_id && ary[1] <= limit) {
	points[count++]=(ary[1] " " baseline);
        print ary[1], baseline; 
        print ary[1], topline;
	last="HUNGRY";
    }
}
match($0, /Second ([0-9]+): philosopher ([0-9]+) is eating for ([0-9]+) seconds/, ary) {
    if (ary[2] == philosopher_id && ary[1] <= limit) {
	points[count++]=(ary[1] " " baseline);
	points[count++]=(ary[1] " " topline);
        print ary[1], topline; 
	if (ary[1] + ary[3] > limit) {
	    ary[3] = limit - ary[1];
	    last="";
        } else
	    last="THINKING";
        points[count++]=((ary[1] + ary[3]) " " topline);
	print ary[1] + ary[3], topline;
	points[count++]=((ary[1] + ary[3]) " " baseline);
        print ary[1] + ary[3], baseline;
    }
}
END {
    if (last == "HUNGRY") {
        print limit, topline;
	print limit, baseline;
    } else if (last == "THINKING") {
        print limit, baseline;
    }
    for (i = count - 1; i >= 0; i--)
	print points[i]
}
