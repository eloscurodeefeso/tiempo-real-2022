#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>

#define PHILOSOPHERS_COUNT 5
#define MAX_THINKING_TIME 3
#define MAX_EATING_TIME 3

#define TIME_MESSAGE_PREFIX "Second %ld: "
#define THINKING_MESSAGE "philosopher %d is thinking for %d seconds\n"
#define HUNGRY_MESSAGE "philosopher %d is hungry\n"
#define EATING_MESSAGE "philosopher %d is eating for %d seconds\n"
#define DONE_MESSAGE "philosopher %d is done eating\n"
#define TAKING_MESSAGE "philosopher %d is taking up forks %d and %d\n"
#define PUTTING_MESSAGE "philosopher %d is putting down forks %d and %d\n"


#define MESSAGE(format, ...) {\
    printf(TIME_MESSAGE_PREFIX format, time(NULL) - start_time, ##__VA_ARGS__);\
    fflush(stdout);}
#define RIGHT(philosopher_id) (philosopher_id + PHILOSOPHERS_COUNT - 1) % PHILOSOPHERS_COUNT
#define LEFT(philosopher_id) (philosopher_id + 1) % PHILOSOPHERS_COUNT

enum Philosopher_State
{
    THINKING,
    HUNGRY,
    EATING
};

void *philosopher_task(void *num);
void take_fork(int);
void put_fork(int);
void test();

sem_t monitor_semaphore;
sem_t philosophers_semaphores[PHILOSOPHERS_COUNT];
enum Philosopher_State philosophers_states[PHILOSOPHERS_COUNT];
int philosophers_ids[PHILOSOPHERS_COUNT];
int *philosophers_queue[PHILOSOPHERS_COUNT];
int queue_start, queue_end;
time_t start_time;

int main()
{
    int i;
    pthread_t thread_id[PHILOSOPHERS_COUNT];
    sem_init(&monitor_semaphore, 0, 1);

    for (i = 0; i < PHILOSOPHERS_COUNT; i++)
    {
        philosophers_ids[i] = i;
        sem_init(&philosophers_semaphores[i], 0, 0);
        philosophers_states[i] = THINKING;
        philosophers_queue[i] = NULL;
    }

    start_time = time(NULL);
    queue_start = 0;
    queue_end = 0;

    for (i = 0; i < PHILOSOPHERS_COUNT; i++)
        pthread_create(&thread_id[i], NULL, philosopher_task, &philosophers_ids[i]);
    for (i = 0; i < PHILOSOPHERS_COUNT; i++)
        pthread_join(thread_id[i], NULL);
}

void *philosopher_task(void *num)
{
    int *i = num;
    while (1)
    {
        int thinking = (rand() % MAX_THINKING_TIME) + 1;
        MESSAGE(THINKING_MESSAGE, *i, thinking);
        sleep(thinking);

        MESSAGE(HUNGRY_MESSAGE, *i);
        take_fork(*i);

        int eating = (rand() % MAX_EATING_TIME) + 1;
        MESSAGE(EATING_MESSAGE, *i, eating);
        sleep(eating);

        MESSAGE(DONE_MESSAGE, *i);
        put_fork(*i);
    }
}

void take_fork(int ph_num)
{
    sem_wait(&monitor_semaphore);
    philosophers_states[ph_num] = HUNGRY;
    philosophers_queue[queue_end++] = &philosophers_ids[ph_num];
    queue_end %= PHILOSOPHERS_COUNT;
    test();
    sem_post(&monitor_semaphore);
    sem_wait(&philosophers_semaphores[ph_num]);
}

void test()
{
    if (queue_start != queue_end)
    {
        int ph_num = *philosophers_queue[queue_start];
        if (philosophers_states[LEFT(ph_num)] != EATING 
        && philosophers_states[RIGHT(ph_num)] != EATING)
        {
            queue_start = (queue_start + 1) % PHILOSOPHERS_COUNT;
            philosophers_states[ph_num] = EATING;
            MESSAGE(TAKING_MESSAGE, ph_num, RIGHT(ph_num), ph_num);
            sem_post(&philosophers_semaphores[ph_num]);
            test();
        }
    }
}

void put_fork(int ph_num)
{
    sem_wait(&monitor_semaphore);
    philosophers_states[ph_num] = THINKING;
    MESSAGE(PUTTING_MESSAGE, ph_num, RIGHT(ph_num), ph_num);
    test();
    sem_post(&monitor_semaphore);
}