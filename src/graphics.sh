#!/bin/bash
if [ $# -ne 2 ]
then
    echo "Usage: graphics.sh <dinner_program> <limit_of_sec>"
    exit 1
fi;
(eval "./$1" | tee -i "$1.txt")
rm -f "$1.dat"
for i in {0..4}
do 
	gawk -v limit="$2" -v philosopher_id=$i -v separator=30 -v height=15 -f graphics.awk "$1.txt" >> "$1.dat"
done
graph -T png -C -q 0.5 -N y -y -10 150 -x 0 "$2" < "$1.dat" > "$1.png"
rm "$1.dat" "$1.txt"
