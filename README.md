# Tiempo Real 2022

Trabajo Práctico final para la materia Sistemas de Tiempo Real dictada en la Universidad Nacional de Tres de Febrero en el ciclo lectivo 2022

## Consigna original

### Trabajo final de STR 2022

**El problema de la cena de los filósofos o problema de los filósofos
cenando** (dining philosophers problem) es un problema clásico de las
ciencias de la computación propuesto por Edsger Dijkstra en 1965 para
representar el problema de la sincronización de procesos en un sistema
operativo. Cabe aclarar que la interpretación está basada en pensadores
chinos, quienes comían con dos palillos, donde es más lógico que se
necesite el del comensal que se siente al lado para poder comer.

![](media/image1.jpg)

_Mesa con 5 platos y 5 palillos_

### Enunciado del problema

Cinco filósofos se sientan alrededor de una mesa y pasan su vida cenando
y pensando. Cada filósofo tiene un plato de arroz y un palillo a la
izquierda de su plato. Para comer el arroz son necesarios dos palillos y
cada filósofo sólo puede tomar los que están a su izquierda y derecha.
Si cualquier filósofo toma un palillo y el otro está ocupado, se quedará
esperando, con el palillo en la mano, hasta que pueda tomar el otro
palillo, para luego empezar a comer.

Si dos filósofos adyacentes intentan tomar el mismo palillo a una vez,
se produce una condición de carrera: ambos compiten por tomar el mismo
palillo, y uno de ellos se queda sin comer.

Si todos los filósofos toman el palillo que está a su derecha al mismo
tiempo, entonces todos se quedarán esperando eternamente, porque alguien
debe liberar el palillo que les falta. Nadie lo hará porque todos se
encuentran en la misma situación (esperando que alguno deje sus
palillos). Entonces los filósofos se morirán de hambre. Este bloqueo
mutuo se denomina interbloqueo o deadlock.

### Algunas definiciones

Cada filósofo es una tarea/proceso y cada palillo es un recurso

Un filósofo puede comer como mínimo durante 200 milisegundos y como
máximo durante un tiempo indefinido pero no puede estar sin comer
durante más de 1 segundo.

Un filósofo debe utilizar dos palillos para comer pero solo puede
agarrar el de la izq y el de la derecha.

### Ejercicio a entregar

El problema consiste en encontrar al menos una solución que permita que
los filósofos nunca se mueran de hambre. Este ejercicio puede tener
varias soluciones.

Se prefiere el uso del SO RTAI, aunque se puede usar un SO común y hacer
una simulación.

El trabajo se puede realizar de manera individual o en grupo de hasta 3
personas.

**Se debe entregar un informe, el código fuente más el ejecutable y un
video de hasta 10 minutos explicando su funcionamiento.**
