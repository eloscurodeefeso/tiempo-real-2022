# Informe Final

## Ingeniería en Computación

### Sistemas de Tiempo Real

#### Trabajo Práctico Final

El informe final en formato latex es posible compilarlo y descargarlo en mediante el servicio LaTeX.Online en https://latexonline.cc/compile?git=https://gitlab.com/eloscurodeefeso/tiempo-real-2022&target=doc/informe.tex&command=pdflatex&force=true 
