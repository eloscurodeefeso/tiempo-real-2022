\documentclass[a4paper]{article}
\usepackage{graphicx}
\usepackage{listings}
\usepackage[utf8]{inputenc}
\usepackage{caption}
\usepackage{lipsum}
\usepackage{enumitem}
\usepackage{xcolor}
\usepackage{float}

\title{Problema de la \\ cena de los filósofos}
\author{Alejandro Araneda – araneda46618@estudiantes.untref.edu.ar}
\date{2do. Cuatrimestre 2022\\Miércoles, 21 de Diciembre}

\def\teacher{Mag. Ing. Claudio Gustavo Aciti
\and Ing. Martín Joel Rodríguez}

\captionsetup{justification=centering,labelsep=period,font={small},%
labelfont=bf,textfont=it}
\renewcommand{\tablename}{Tabla}
\renewcommand{\figurename}{Figura}
\renewcommand{\abstractname}{Resumen}
\renewcommand{\refname}{Referencias}
\let\originalcite\cite
\renewcommand{\cite}[2][]{\textsuperscript{\originalcite{#2}}}
% Cambiamos el estilo de las citas bibliograficas
\makeatletter\let\@afterindentfalse\@afterindenttrue\makeatother
% Para indentar primer parrafo
\setcounter{secnumdepth}{0}
% No se numeran las secciones pero si estan en el TOC 
\lstdefinestyle{console}{
    backgroundcolor=\color{black},
    basicstyle=\normalsize\color{white}\ttfamily
}
\newfloat{lstfloat}{h}{lop}
\floatname{lstfloat}{Listado}
%Apéndices como Anexos en páginas separadas y numeración alfabética 
\let\originalappendix\appendix
\renewcommand{\appendix}{%
    \newpage\originalappendix\pagenumbering{gobble}%
    \renewcommand\thesection{Anexo \Alph{section}}
    \setcounter{secnumdepth}{1}
}

\begin{document}

\begin{titlepage}\renewcommand\and\par\centering\makeatletter
    \includegraphics{logo.png}\par
    {\Large Ingeniería en Computación \par}\vspace{0.5cm}
    {\LARGE Sistemas de Tiempo Real \par}\vfill
    {\huge \@title \par}\vfill
    Alumno:\par
    \@author\vfill
    Fecha de entrega:\par
    \@date\vfill
    Docentes:\par
    \teacher\vspace{1cm}\makeatother
\end{titlepage}

\begin{abstract}

    En el presente trabajo se abordará el clásico problema de la cena de los filósofos
    implementando alguna de sus posibles soluciones y analizándolas con respecto a la
    problemática de la sincronización de procesos y su relevancia en el contexto de los 
    sistemas de tiempo real.

\end{abstract}

\section{Introducción}

El problema de la cena de los filósofos (``the dining philosephers problem'', en inglés) es un problema clásico en ciencias de la computación, formulado primeramente por Edsger Dijkstra\cite{bib:dijkstra} y que aborda diversas cuestiones del procesamiento concurrente.

\begin{figure}[h]\centering
    \includegraphics[height=3.5cm]{cena.png}
    \caption{Ilustración original del problema de la cena de los \\ filósofos en el \textit{paper} de Dijkstra.}
    \label{fig:cena}
\end{figure}

De acuerdo a la formulación original dada por el Dijkstra:

\begin{quotation}
Cinco filósofos, numerados del 0 al 4, viven en una casa donde 
la mesa está puesta para ellos y cada filósofo tiene su propio lugar en la mesa. Su único problema, además de los de la filosofía, es que el plato servido es
un tipo de espagueti muy difícil, que hay que comerlo con dos tenedores. Hay
dos tenedores al lado de cada plato, por lo que no presenta dificultad: como consecuencia,
sin embargo, dos vecinos no pueden comer al mismo tiempo.
\end{quotation} 

La literatura reconoce varias soluciones al problema, las que se distinguen por la forma en la que tratan la cuestión de los \textit{deadlocks}, ``abrazo mortal'' o interbloqueo (un conjunto de procesos en un estado de espera tal que ninguno de ellos tiene suficientes criterios para continuar su ejecución), 
la \textit{starvation} o ``inanición'' (a un proceso se le niegan perpetuamente los recursos necesarios para procesar su trabajo) y una política justa (\textit{fairness}) o balanceada del algoritmo de sincronización.

Se reconocen cuatro características en la concurrencia de procesos que de suceder todas juntas producen (o pueden producir, en el caso de algoritmos no determinísticos) el interbloqueo:

\begin{description}
\item[exclusión mutua:] cada proceso debe obtener acceso mutuamente exclusivo a un subconjunto de la variables de estado que definen la condición de progreso

\item[retener y esperar:] cada proceso debe permanecer dentro de su región de exclusión mutua mientras espera el resto de las condiciones de progreso

\item[espera circular:] es posible establecer un circuito cerrado de procesos que comparten variables de estado como condición de progreso

\item[no apropiación:] un proceso permanecerá en su región critica y no podrá ser removido de ella por ningún artefacto externo hasta tanto no se cumplan con las condiciones de progreso.
\end{description}

La solución propuesta originalmente por Dijkstra resuelve el interbloqueo por espera circular mediante la imposición de una jerarquía a los recursos, en particular, un orden parcial: numera los tenedores del 0 al 4 al igual que los filósofos y obliga a los filósofos a requerir los tenedores siempre en un orden (los de mayor jerarquía primero, por ejemplo) y devolverlos en la forma inversa.

\begin{lstfloat}
\begin{lstlisting}[style=console]
$ lshw
latitude5290                
    description: Notebook
    product: Latitude 5290 (0815)
    vendor: Dell Inc.
    serial: 8NT0KR2
    width: 64 bits
  *-core
     *-cpu
          description: CPU
          product: Intel(R) Core(TM) i5-8250U CPU @ 1.60GHz
          vendor: Intel Corp.
          physical id: 50
          bus info: cpu@0
          version: 6.142.10
          serial: To Be Filled By O.E.M.
          slot: U3E1
          size: 2687MHz
          capacity: 3400MHz
          width: 64 bits
          clock: 100MHz
          configuration: cores=4 enabledcores=4 microcode=240 threads=8
\end{lstlisting}
\caption{Salida por consola (parcial) del comando \texttt{\emph{lshw}} para el equipo utilizado.}
\label{lst:hardware}
\end{lstfloat}

La solución propuesta por Tanenbaum\cite{bib:tanenbaum}, en cambio, evita el interbloqueo al eliminar de su protocolo de sincronización la posibilidad de retener un recurso mientras se espera por el resto de las condiciones de progreso: mediante un ``camarero'', para seguir la metáfora de la cena, implementado con un monitor (un objeto que asocia métodos y estados dentro de una región crítica de exclusión mutua) un filósofo está habilitado a comer sólo si puede requerir ambos tenedores a la vez, lo que es igual a decir que sus filósofos vecinos no están comiendo.

Si bien ambas soluciones resuelven el problema del interbloqueo, ninguna está libre de inanición. En el caso de la solución de Dijkstra, es posible presentar una situación en la que dos filósofos no adyacentes realizan más rápidamente las acciones de pensar y comer que el vecino filósofo al que rodean, de manera que éste último no puede acceder a comer a pesar de que el ciclo progrese para todos los demás. Para el caso de la solución de Tanenbaum, la configuración que habilita la inanición es más compleja como lo demuestra Gingras\cite{bib:gingras}.

Existen más soluciones, algunas tan complejas como la de Chandy y Misra\cite{bib:misra} que además de resolver la inanición permiten su implementación en sistemas distribuidos utilizando el envío de mensajes entre procesos y administrando localmente los recursos con un protocolo que agrega una señal de estado a los tenedores (``sucios'' o ``limpios''). En este trabajo se emplea una variante del protocolo de sincronización de Gingras, que utiliza una cola para ir aumentando la prioridad de acceso a los recursos a aquellos filósofos que estén más ``hambrientos'', es decir que hayan permanecido bloqueados por más tiempo. Distintos algoritmos de administración de dicha cola impactan en la performance (tomando como métrica el tiempo promedio de bloqueo, por ejemplo) como lo demuestra Kwok-bun Yue\cite{bib:yue}.
 
\section{Recursos}

El trabajo se desarrolla en una laptop de la marca Dell, multinacional de origen estadounidense, con la distribución Ubuntu del sistema operativo GNU/Linux mantenida por la empresa Canonical con sede en el Reino Unido.

La solución se implemente en el lenguaje C compatible con su versión C99 (ISO/IEC 9899:1999) con librerías que cumplimentan el estándar POSIX (IEEE Std 1003.2-1992) para el uso de semáforos e hilos.

\begin{lstfloat}
\begin{lstlisting}[style=console]
$ uname -a
Linux Latitude5290 5.15.0-56-generic 
#62-Ubuntu SMP Tue Nov 22 19:54:14 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
$ gcc -v
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/11/lto-wrapper
OFFLOAD_TARGET_NAMES=nvptx-none:amdgcn-amdhsa
OFFLOAD_TARGET_DEFAULT=1
Target: x86_64-linux-gnu
Thread model: posix
Supported LTO compression algorithms: zlib zstd
gcc version 11.3.0 (Ubuntu 11.3.0-1ubuntu1~22.04) 
\end{lstlisting}
\caption{Salida por consola de los comandos \texttt{\emph{uname}} y \texttt{\emph{gcc}} en el sistema utilizado.}
\label{lst:software}
\end{lstfloat}

El código fuente se lista completo en el Anexo A y los binarios se producen con la suite de compilación GCC del proyecto GNU.

\section{Solución}\label{sec:solution}

La solución que se implementa pertenece a la familia de aquellos protocolos de sincronización que resuelven el 
interbloqueo evitando la acción de retener un recurso a la vez que se espera por las demás condiciones de progreso. Esto se logra mediante la implementación de un monitor: un 
objeto que vincula estados y funciones que los hilos o 
procesos a sincronizar utilizarán uno por vez dentro de un zona de exclusión mutua. 

\begin{figure}[h]\centering
    \includegraphics[height=5cm]{monitor.png}
    \caption{Diagrama UML del objeto monitor.}
    \label{fig:monitor}
\end{figure}

La responsabilidad del monitor es la de entregar todos los
recursos necesarios juntos, y si no es posible, mantener
bloqueado al hilo. Su interfaz pública (las funciones \texttt{take\_fork} y \texttt{put\_fork}) son accedidas por 
los distintos hilos, pero su implementación
dentro de la región crítica delimitada por el 
uso del semáforo del monitor (el atributo privado \texttt{monitor\_semaphore}) nos permite su sincronización.

\begin{lstfloat}
\lstinputlisting[language=C, firstline=103, lastline=117]{../src/dinner.c}
\caption{Función privada del monitor que verifica que la cabeza de la cola de bloqueados esté en condiciones de continuar.}
\label{lst:queue}
\end{lstfloat}

El corazón de la solución se encuentra en el método
privado del monitor, la función \texttt{test}, que se 
extracta en el Listado \ref{lst:queue}. Allí se verifica
que la cola, implementada simplemente como un \textit{buffer} circular, no esté vacía y que no se hallen comiendo los filósofos vecinos del que se encuentra a la cabeza de la misma. Esto significa que están dados todos los recursos necesarios para que este hilo bloqueado continúe. Como detalle se hace notar que para mejorar la performance, 
una vez desencolado y modificado el estado del hilo, se repite recursivamente el proceso sobre la nueva cabeza de la cola.

\section{Análisis}

El formato de salida que obtenemos al compilar es el
habitual para los estudios de la temática, agregando un 
marcador del tiempo transcurrido (en segundos) desde el
inicio del programa, a los efectos de poder realizar las
gráficas comparativas.

\begin{lstfloat}
\lstinputlisting[style=console, firstline=0, lastline=10]{dinner.txt}
\caption{Salida por consola del ejecutable \texttt{\emph{dinner}} obtenido mediante la compilación con \texttt{\emph{gcc -pthread dinner.c -o dinner}}.}
\label{lst:output}
\end{lstfloat}

Una representación gráfica de la salida del programa se
presenta en la Figura \ref{fig:graph} (izquierda) gracias a las herramienta \texttt{graph} del 
paquete de aplicaciones \\ \texttt{plotutils} del proyecto GNU. Previo se ha transformado el resultado en un \textit{dataset} mediante un \textit{script} del lenguaje
\texttt{awk} y luego se ha generado la imagen, todo con el \textit{script} del lenguaje \texttt{bash} que se adjunta. Posteriormente se ha retocado la imagen por cuestiones estéticas.

\begin{figure}[h]\centering
    \includegraphics[height=6.5cm]{dinner.png}\includegraphics[height=6.5cm]{tanenbaum.png}
    \caption{Representaciones gráficas de la ejecución de la solución presentada (izquierda) y la del libro de Tanenbaum (derecha), con los hilos identificados por colores, lleno el tiempo del bloqueo, y en las abscisa el eje del tiempo en segundos.}
    \label{fig:graph}
\end{figure}

Para una mejor comparativa, también se ofrece la implementación de la solución de Tanenbaum, fielmente traslada del libro, a la cual se le realiza el mismo 
procedimiento para obtener la Figura \ref{fig:graph} (derecha). Como se aprecia, es más probable para el 
caso de la solución de Tanenbaum que los hilos 
vuelvan a obtener los recursos apenas terminados de utilizar, lo que genera que un hilo que ya se encuentre
bloqueado prolongue aun más su condición, al menos comparándolo
con el caso de la solución que propone este trabajo.

\section{Conclusiones}

La relevancia del uso de recursos compartidos en Sistemas 
de Tiempo Real no sólo se reducen al caso de la memoria, como las colas y diversas estructuras de datos en arquitecturas productor-consumidor, sino especialmente también en el caso de \textit{software} implantado en computadores en un módulo (ya sea con unidades de procesamiento implementadas con microcontroladores o microprocesadores) donde es común que varios puertos generales de 
entrada/salida (GPIO) para señales analógicas compartan un
mismo módulo de conversión analógico/digital multiplexado.

Toda vez que nos apartamos de una planificación estática
de tareas para responder a eventos externos, se hace aun
más necesaria la correcta elección de un protocolo de 
sincronización que nos permita no sólo garantizar el
cumplimiento de las tareas dentro de su \textit{deadline}
especificado, para los sistemas \textit{hard real-time},
sino también poder administrar los tiempos de bloqueo 
para poder presentar un cumplimiento degrado de las tareas, 
en el caso de los sistemas \textit{soft real-time}.

Como se ha visto, en tanto se eviten alguna de las características que generan interbloqueo (exclusión mutua, retener y esperar, espera circular o no apropiación) es posible hallar solución en general al problema de sincronización y uso compartido mediante semáforos (o mutex o monitores) ya que no sólo son implementados en sistemas
operativos de uso general sino también en sistemas de tiempo real como el \textit{patch} RTAI para Linux o en FreeRTOS. En cambio, se vuelve más complejo cumplimentar 
con el requerimiento de evitar la inanición y es aquí
donde la elección de un protocolo de sincronización 
específico puede hacer la diferencia no sólo en performance
sino incluso en la utilidad misma del sistema.

\noindent\rule{\textwidth}{1pt}

\begin{thebibliography}{9}

\bibitem{bib:misra}
\textsc{Chandy K. M. \& Misra J.} (1984).
\textit{``Drinking Philosophers Problem''}.
ACM Transactions on Programming Languages and Systems (TOPLAS), Vol. 6, No. 4, October 1984, Pages 632-646. 

\bibitem{bib:dijkstra}
\textsc{Dijkstra, E. W.} (1971). 
\textit{``Hierarchical ordering of sequential processes''}. Acta Informatica, 1(2), 115–138. \texttt{doi:10.1007/bf00289519}.

\bibitem{bib:gingras}
\textsc{Gingras, A. R.} (1990).
\textit{``Dining Philosophers Revisited''}.
ACM Special Interest Group on Computer Science Education (SIGCSE) Bulletin, Vol. 22, No. 3, September 1990

\bibitem{bib:tanenbaum}
\textsc{Tanenbaum, A. S.} (1987). 
\textit{``Operating Systems: Design and Implementation''}. Englewood Cliffs, New Jersey: Prentice-Hall Inc.

\bibitem{bib:yue}
\textsc{Kwok-bun Yue} (1991).
\textit{``Dining Philosophers Revisited, Again''}.
ACM Special Interest Group on Computer Science Education (SIGCSE) Bulletin, Vol. 23, No. 2, June 1991

\end{thebibliography}

\appendix

\section{}
A continuación listamos en extenso el código fuente en lenguaje C correspondiente a la solución implementada. Su explicación se encuentra en la Sección Solución.

\lstinputlisting[language=C,basicstyle=\ttfamily\scriptsize,numbers=left]{../src/dinner.c}

\end{document}
